import { Request, Response } from 'express';

export default class ContactRoutes {
  public routes(app): void {
    app
      .route('/contact')
      .get((req: Request, res: Response) => {
        res.status(200).send({
          message: 'GET success',
        });
      })
      .post((req: Request, res: Response) => {
        res.status(200).send({
          message: 'POST success',
        });
      });
    app
      .route('/contact/:id')
      .get((req: Request, res: Response) => {
        res.status(200).send({
          message: 'GET success',
        });
      })
      .put((req: Request, res: Response) => {
        res.status(200).send({
          message: 'PUT success',
        });
      })
      .delete((req: Request, res: Response) => {
        res.status(200).send({
          message: 'DELETE success',
        });
      });
  }
}
