import { Request, Response } from "express";
import { ContactCtrl } from "../controllers/ContactCtrl";

export class Routes {
    public contactController: ContactCtrl = new ContactCtrl();
    public routes(app): void {
        app.route("/").get((req: Request, res: Response) => {
            res.status(200).send({
                message: "GET success"
            });
        });
        app
            .route("/contact")
            .get(this.contactController.getContacts)
            .post(this.contactController.addNewContact);
        app
            .route("/contact/:id")
            .get(this.contactController.getContactByID)
            .put(this.contactController.updateContact)
            .delete(this.contactController.deleteContact);
    }
}
