import * as mongoose from 'mongoose';
import { Request, Response } from 'express';

import { Contact } from '../models';

export class ContactCtrl {
  public addNewContact(req: Request, res: Response) {
    let newContact = new Contact(req.body);

    newContact.save((err, contact) => {
      if (err) {
        res.send(err);
      }
      res.json(contact);
    });
  }

  public getContacts(req: Request, res: Response) {
    Contact.find({}, (err, contact) => {
      if (err) {
        res.send(err);
      }
      res.json(contact);
    });
  }

  public getContactByID(req: Request, res: Response) {
    Contact.findById(req.params.id, (err, contact) => {
      if (err) {
        res.send(err);
      }
      res.json(contact);
    });
  }

  public updateContact(req: Request, res: Response) {
    Contact.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true },
      (err, contact) => {
        if (err) {
          res.send(err);
        }
        res.json(contact);
      }
    );
  }

  public deleteContact(req: Request, res: Response) {
    Contact.remove({ _id: req.params.id }, err => {
      if (err) {
        res.send(err);
      }
      res.json({
        message: 'Successfully deleted contact!',
      });
    });
  }
}
