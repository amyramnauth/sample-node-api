import * as mongoose from "mongoose";
const DB_HOST = process.env.DATABASE;
const DB_URI = `mongodb://${DB_HOST}/app`;

export const connect = async () =>
    mongoose.connect(DB_URI).then(
        () => {
            console.log("Connection Succeeded");
        },
        err => {
            console.error.bind(console, "connection error");
        }
    );
