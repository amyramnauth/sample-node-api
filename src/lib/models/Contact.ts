import * as mongoose from 'mongoose';
import { ContactSchema } from '../schemas/Contact';

const Contact = mongoose.model('Contact', ContactSchema);

export default Contact;
