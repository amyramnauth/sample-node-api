import app from "./app";
import * as db from "./db";
import * as dotenv from "dotenv";

dotenv.load();

const PORT = process.env.PORT;

const start = async () => {
    await db.connect();
    app.listen(PORT, (): void => {
        console.log(`Express server listening on port ${PORT}`);
    });
};

start();
